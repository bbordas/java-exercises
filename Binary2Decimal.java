package exercises;
import java.util.Scanner;

public class Binary2Decimal {
    public static void main (String[] args)
    {
        System.out.print("Please enter a number in binary: ");

        Scanner scan = new Scanner(System.in);
        String binaryString = scan.nextLine();

        int bLength = binaryString.length();

        int binaryNumber = 0;
        boolean flag = false;

        for(int position = 0; position <bLength; position++)
        {
            int num = binaryString.charAt(position)-'0';
            int order = bLength - 1 - position;

            switch(num)
            {
                case 0:
                    break;
                case 1:
                    binaryNumber = binaryNumber +(int)(Math.pow(2,(order)));
                    break;
                default:
                    flag = true;
                    break;
            }
        }
        if(flag)
        {
            System.out.print("Invalid binary string: " + binaryString);
        }
        else
        {
            System.out.print(binaryNumber);
        }
    }
}
